const urlParser = require('url');
const { send } = require('micro');
const { router, get } = require('microrouter');
const { getNFEData } = require('./crawler');

const notfound = (req, res) => send(res, 404, 'Not found route');

const crawler = async (req, res) => {
    try {
        const url = decodeURIComponent(req.query.url)
        const nfe = await getNFEData(url);
        send(res, 200, nfe);
    } catch (error) {
        send(res, 500, error); // review corrent error code
    }
}

module.exports = router(get('/nfce', crawler), get('/*', notfound));

