const request = require('request');

// const url = "http://nfe.sefaz.ba.gov.br/servicos/nfce/modulos/geral/NFCEC_consulta_chave_acesso.aspx?chNFe=29170847508411089203653290000453901000038336&nVersao=100&tpAmb=1&dhEmi=323031372d30382d32365431373a35343a35362d30333a3030&vNF=318.84&vICMS=17.68&digVal=5033716b37657962647a31426f515353756f682f423542657671633d&cIdToken=000001&cHashQRCode=06fb5e48c5356f4c656a659aef94a530a7c0aecd";
const url = "http://nfe.sefaz.ba.gov.br/servicos/nfce/modulos/geral/NFCEC_consulta_chave_acesso.aspx?chNFe=29180273849952000239650200000289791558644260&nVersao=100&tpAmb=1&cDest=02441251554&dhEmi=323031382d30322d30365432303a30313a31342d30333a3030&vNF=592.40&vICMS=83.84&digVal=55474f534c485556563573336d51582f71493244584f34765742553d&cIdToken=000001&cHashQRCode=F053B8F7F42A73F34A3063AAA9B53D1E5A509F96";
const host = 'http://localhost:3000/nfce?url=';
const endpoint = `${host}${url}`;

request({
  method: 'GET',
  uri: endpoint,
  time: true
}, function(error, response, body) {
  console.log(body);
  console.log(response.elapsedTime);
});
