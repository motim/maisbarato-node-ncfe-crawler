const puppeteer = require('puppeteer');

async function getSelectorContent(page, selectorString){
    const value = await page.evaluate(selectorString => {
        const anchors = Array.from(document.querySelectorAll(selectorString));
        return anchors.map(anchor => anchor.textContent.replace(/[\n\r]+/g, '').trim());
    }, selectorString);
    return new Promise((resolve, reject) => {
        resolve(value);
    });
}

async function getXpathContent(page, xpathSelector) {
    const xpathData = await page.$x(xpathSelector);
    // return Promise.all(
    //     xpathData.map(async (xdata) => {
    //         const xpathTextContent = await xdata.getProperty('textContent');
    //         const xpathContent = await xpathTextContent.jsonValue();
    //         return xpathContent;
    //     })
    // );

    const arrayData = [];
    for (const xdata of xpathData) {
        const xpathTextContent = await xdata.getProperty('textContent');
        const xpathContent = await xpathTextContent.jsonValue();
        arrayData.push(xpathContent);
    }
    return new Promise((resolve, reject) => {
        resolve(arrayData);
    });
}

async function getEmitenteData(page) {
    const razaoSocialSelector = "#Emitente > table:nth-child(2) > tbody > tr.col-2 > td:nth-child(1) > span";
    const razaoSocial = await getSelectorContent(page, razaoSocialSelector);

    const nomeFantasiaSelector = "#Emitente > table:nth-child(2) > tbody > tr.col-2 > td:nth-child(2) > span";
    const nomeFantasia = await getSelectorContent(page, nomeFantasiaSelector);
    
    const cnpjSelector = "#Emitente > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(1) > span";
    const cnpj = await getSelectorContent(page, cnpjSelector);
    
    const bairroSelector = "#Emitente > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(1) > span";
    const bairro = await getSelectorContent(page, bairroSelector);
    
    const enderecoSelector = "#Emitente > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(2) > span";
    const endereco = await getSelectorContent(page, enderecoSelector);
    
    const cepSelector = "#Emitente > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(2) > span";
    const cep = await getSelectorContent(page, cepSelector);
    
    const municipioSelector = "#Emitente > table:nth-child(2) > tbody > tr:nth-child(4) > td:nth-child(1) > span";
    const municipio = await page.evaluate(municipioSelector => {
        const anchors = Array.from(document.querySelectorAll(municipioSelector));
        return anchors.map(anchor => {
            const text = anchor.textContent;
            const codCidade = text.split('-')[0].trim();
            const cidade = text.split('-')[1].trim();
            return {
                'nome': cidade,
                'numero': codCidade
            }
        });
    }, municipioSelector);

    return new Promise((resolve, reject) => {
        resolve({
            razao_social: razaoSocial[0],
            nome_fantasia: nomeFantasia[0],
            cnpj: cnpj[0],
            municipio: municipio[0],
            bairro: bairro[0],
            endereco: endereco[0],
            cep: cep[0]
        });
    });
}

async function getNfe(page) {
    const modelo = await getSelectorContent(page, "#NFe > table:nth-child(2) > tbody > tr > td:nth-child(1) > span");
    const serie = await getSelectorContent(page, "#NFe > table:nth-child(2) > tbody > tr > td:nth-child(2) > span");
    const numero = await getSelectorContent(page, "#NFe > table:nth-child(2) > tbody > tr > td:nth-child(3) > span");
    const chaveAcesso = await getSelectorContent(page, "#lbl_chave_acesso");
    
    const dateTimeSelector = "#NFe > table:nth-child(2) > tbody > tr > td:nth-child(4) > span";
    const dateTime = await page.evaluate(dateTimeSelector => {
        const anchors = Array.from(document.querySelectorAll(dateTimeSelector));
        return anchors.map(anchor => {
            const text = anchor.textContent;
            const dateAndTime = text.split(' ');
            const date = dateAndTime[0].trim();
            const hour = dateAndTime[1].trim();
            return {
                'date': date,
                'hour': hour
            }
        });
    }, dateTimeSelector);

    const emitenteCNPJ = await getSelectorContent(page, "#NFe > table:nth-child(4) > tbody > tr > td.fixo-nfe-cpf-cnpj > span");
    const emitenteRazao = await getSelectorContent(page, "#NFe > table:nth-child(4) > tbody > tr > td:nth-child(2) > span");
    const emitenteIE = await getSelectorContent(page, "#NFe > table:nth-child(4) > tbody > tr > td.fixo-nfe-iest > span");
    const emitenteUF = await getSelectorContent(page, "#NFe > table:nth-child(4) > tbody > tr > td.fixo-nfe-uf > span");

    return new Promise((resolve, reject) => {
        resolve({
            modelo: modelo[0],
            serie: serie[0],
            numero: numero[0],
            data: dateTime[0].date,
            hora: dateTime[0].hour,
            chave_acesso: chaveAcesso[0],
            emitente: {
                cnpj: emitenteCNPJ[0],
                razao_social: emitenteRazao[0],
                ie: emitenteIE[0],
                uf: emitenteUF[0]
            }
        });
    });
}

async function mergeEmitenteParaNfe(nfe, emitente){
    nfe['emitente']['municipio'] = emitente['municipio'];
    nfe['emitente']['bairro'] = emitente['bairro'];
    nfe['emitente']['endereco'] = emitente['endereco'];
    nfe['emitente']['cep'] = emitente['cep'];
    nfe['emitente']['nome_fantasia'] = emitente['nome_fantasia'];
    
    return new Promise((resolve, reject) => {
        resolve(nfe);
    });
}

async function getProdutos(page){
    const descricoes = await getSelectorContent(page, "table.textoArial8:nth-child(1) div.Formulario:nth-child(2) td.table_produtos table.toggle tbody:nth-child(1) tr.highlighted td.fixo-prod-serv-descricao > span.multiline:nth-child(2)");
    const qtds = await getSelectorContent(page, "table.textoArial8:nth-child(1) div.Formulario:nth-child(2) td.table_produtos table.toggle tbody:nth-child(1) tr.highlighted td.fixo-prod-serv-qtd > span.linha:nth-child(2)");
    const unidadesComerciais = await getSelectorContent(page, "table.textoArial8:nth-child(1) div.Formulario:nth-child(2) td.table_produtos table.toggle tbody:nth-child(1) tr.highlighted td.fixo-prod-serv-uc > span.linha:nth-child(2)");
    const valores = await getSelectorContent(page, "table.textoArial8:nth-child(1) div.Formulario:nth-child(2) td.table_produtos table.toggle tbody:nth-child(1) tr.highlighted td.fixo-prod-serv-vb > span.linha:nth-child(2)");
    const valoresUnitarios = await getSelectorContent(page, "table.textoArial8:nth-child(1) div.Formulario:nth-child(2) td.table_produtos table.toggable table:nth-child(2) tbody:nth-child(1) tr:nth-child(4) td:nth-child(1) > span.linha:nth-child(2)");
    const codigos = await getSelectorContent(page, "table.textoArial8:nth-child(1) div.Formulario:nth-child(2) td.table_produtos table.toggable table:nth-child(1) tbody:nth-child(1) tr.col-4:nth-child(1) td:nth-child(1) > span.linha:nth-child(2)");
    const ncms = await getSelectorContent(page, "table.textoArial8:nth-child(1) div.Formulario:nth-child(2) td.table_produtos table.toggable table:nth-child(1) tbody:nth-child(1) tr.col-4:nth-child(1) td:nth-child(2) > span.linha:nth-child(2)");
    const cfops = await getXpathContent(page, `//div[@id='Prod']/div/table/tbody/tr/td[@class='table_produtos']/table/tbody/tr/td/table[1]/tbody/tr[2]/td[2]/span[@class='linha']`);
    const eansComerciais = await getXpathContent(page, `//div[@id='Prod']/div/table/tbody/tr/td[@class='table_produtos']/table/tbody/tr/td/table/tbody/tr[@class='col-3']/td[1]/span[@class='linha']`);
    const eansTributaveis = await getXpathContent(page, `//div[@id='Prod']/div/table/tbody/tr/td[@class='table_produtos']/table/tbody/tr/td/table[2]/tbody/tr[3]/td[1]/span[@class='linha']`);
    const descontos = await getSelectorContent(page, "table.textoArial8:nth-child(1) div.Formulario:nth-child(2) td.table_produtos table.toggable table:nth-child(1) tbody:nth-child(1) tr:nth-child(3) td:nth-child(1) > span.linha:nth-child(2)");

    const produtos = [];
    for (let index = 0; index < descricoes.length; index++) {
        produtos.push({
            'descricao': descricoes[index],
            'qtd': qtds[index] ,
            'unidade_comercial': unidadesComerciais[index],
            'valor': valores[index],
            'valor_unitario': valoresUnitarios[index],
            'codigo': codigos[index],
            'ncm': ncms[index],
            'cfop': cfops[index],
            'ean_comercial': eansComerciais[index],
            'ean_tributavel': eansTributaveis[index],
            'desconto': descontos[index],
        })
    }
    return new Promise((resolve, reject) => {
        resolve(produtos);
    });
}

(async () => {
    const url = "http://nfe.sefaz.ba.gov.br/servicos/nfce/modulos/geral/NFCEC_consulta_chave_acesso.aspx?chNFe=29170847508411089203653290000453901000038336&nVersao=100&tpAmb=1&dhEmi=323031372d30382d32365431373a35343a35362d30333a3030&vNF=318.84&vICMS=17.68&digVal=5033716b37657962647a31426f515353756f682f423542657671633d&cIdToken=000001&cHashQRCode=06fb5e48c5356f4c656a659aef94a530a7c0aecd";
    
    const startTime = new Date();

    const browser = await puppeteer.launch({slowMo: 2});
    const page = await browser.newPage();
    await page.setViewport({
        width: 1024,
        height: 860
    })
    await page.goto(url);

    let [response] = await Promise.all([
        page.waitForNavigation(),
        page.click('#btn_visualizar_abas')
    ]);

    let nfe = await getNfe(page);

    await page.click('#btn_aba_emitente')
    const razaoSocialSelector = "#Emitente > table:nth-child(2) > tbody > tr.col-2 > td:nth-child(1) > span";
    await page.waitForSelector(razaoSocialSelector);
    const emitente = await getEmitenteData(page);

    nfe = await mergeEmitenteParaNfe(nfe, emitente);

    await page.click('#btn_aba_produtos')
    const firstProductNumberSelector = "#Prod > div > table:nth-child(1) > tbody > tr > td > table.toggle > tbody > tr > td.fixo-prod-serv-numero > span";
    await page.waitForSelector(firstProductNumberSelector);

    const produtos = await getProdutos(page);
    nfe['produtos'] = produtos;
    
    const endTime = new Date();
    console.log((endTime - startTime) / 1000);
    console.log(nfe);
    // console.log(produtos);

    await browser.close();
})();