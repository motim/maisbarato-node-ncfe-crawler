const puppeteer = require('puppeteer');

const NOT_FOUND_INVOICE_ERROR_MSG = 'Esse cupom ainda não foi enviado para a Sefaz. Pode deixar que a gente cuida disso depois. 😉';

async function getSelectorContent(page, selectorString) {
    return page.evaluate(selector => {
        const anchors = Array.from(document.querySelectorAll(selector));
        return Promise.resolve(anchors.map(anchor => anchor.textContent.replace(/[\n\r]+/g, '').trim()));
    }, selectorString);
}

async function getXpathContent(page, xpathSelector) {
    const xpathData = await page.$x(xpathSelector);
    const arrayData = [];
    for (const xdata of xpathData) {
        const xpathTextContent = await xdata.getProperty('textContent');
        const xpathContent = await xpathTextContent.jsonValue();
        arrayData.push(xpathContent);
    }
    return new Promise((resolve, reject) => {
        resolve(arrayData);
    });
}

async function getEmitenteData(page) {
    const razaoSocialPromise = getSelectorContent(page, "#Emitente > table:nth-child(2) > tbody > tr.col-2 > td:nth-child(1) > span");

    const nomeFantasiaPromise = getSelectorContent(page, "#Emitente > table:nth-child(2) > tbody > tr.col-2 > td:nth-child(2) > span");

    const cnpjPromise = getSelectorContent(page, "#Emitente > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(1) > span");

    const bairroPromise = getSelectorContent(page, "#Emitente > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(1) > span");

    const enderecoPromise = getSelectorContent(page, "#Emitente > table:nth-child(2) > tbody > tr:nth-child(2) > td:nth-child(2) > span");

    const cepPromise = getSelectorContent(page, "#Emitente > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(2) > span");

    const municipioPromise = page.evaluate(municipioSelector => {
        const anchors = Array.from(document.querySelectorAll(municipioSelector));
        return anchors.map(anchor => {
            const text = anchor.textContent;
            const codCidade = text.split('-')[0].trim();
            const cidade = text.split('-')[1].trim();
            return {
                'nome': cidade,
                'numero': codCidade
            }
        });
    }, "#Emitente > table:nth-child(2) > tbody > tr:nth-child(4) > td:nth-child(1) > span");

    const razaoSocial = await razaoSocialPromise;
    const nomeFantasia = await nomeFantasiaPromise;
    const cnpj = await cnpjPromise;
    const bairro = await bairroPromise;
    const endereco = await enderecoPromise;
    const cep = await cepPromise;
    const municipio = await municipioPromise;

    return new Promise((resolve, reject) => {
        resolve({
            razao_social: razaoSocial[0],
            nome_fantasia: nomeFantasia[0],
            cnpj: cnpj[0],
            municipio: municipio[0],
            bairro: bairro[0],
            endereco: endereco[0],
            cep: cep[0]
        });
    });
}

async function getNfe(page) {
    const modeloPromise = getSelectorContent(page, "#NFe > table:nth-child(2) > tbody > tr > td:nth-child(1) > span");
    const seriePromise = getSelectorContent(page, "#NFe > table:nth-child(2) > tbody > tr > td:nth-child(2) > span");
    const numeroPromise = getSelectorContent(page, "#NFe > table:nth-child(2) > tbody > tr > td:nth-child(3) > span");
    const chaveAcessoPromise = getSelectorContent(page, "#lbl_chave_acesso");
    const valorPromise = getSelectorContent(page, "#NFe > table:nth-child(2) > tbody > tr > td:nth-child(6) > span");

    const dateTimePromise = page.evaluate(dateTimeSelector => {
        const anchors = Array.from(document.querySelectorAll(dateTimeSelector));
        return anchors.map(anchor => {
            const text = anchor.textContent;
            const dateAndTime = text.split(' ');
            const date = dateAndTime[0].trim();
            const hour = dateAndTime[1].trim();
            return {
                'date': date,
                'hour': hour
            }
        });
    }, "#NFe > table:nth-child(2) > tbody > tr > td:nth-child(4) > span");

    const emitenteCNPJPromise = getSelectorContent(page, "#NFe > table:nth-child(4) > tbody > tr > td.fixo-nfe-cpf-cnpj > span");
    const emitenteRazaoPromise = getSelectorContent(page, "#NFe > table:nth-child(4) > tbody > tr > td:nth-child(2) > span");
    const emitenteIEPromise = getSelectorContent(page, "#NFe > table:nth-child(4) > tbody > tr > td.fixo-nfe-iest > span");
    const emitenteUFPromise = getSelectorContent(page, "#NFe > table:nth-child(4) > tbody > tr > td.fixo-nfe-uf > span");

    const modelo = await modeloPromise;
    const serie = await seriePromise;
    const numero = await numeroPromise;
    const chaveAcesso = await chaveAcessoPromise;
    const dateTime = await dateTimePromise;
    const emitenteCNPJ = await emitenteCNPJPromise;
    const emitenteRazao = await emitenteRazaoPromise;
    const emitenteIE = await emitenteIEPromise;
    const emitenteUF = await emitenteUFPromise;
    const valor = await valorPromise;

    return new Promise((resolve, reject) => {
        resolve({
            modelo: modelo[0],
            serie: serie[0],
            numero: numero[0],
            data: dateTime[0].date,
            hora: dateTime[0].hour,
            chave_acesso: chaveAcesso[0],
            emitente: {
                cnpj: emitenteCNPJ[0],
                razao_social: emitenteRazao[0],
                ie: emitenteIE[0],
                uf: emitenteUF[0]
            },
            valor: valor[0]
        });
    });
}

async function mergeEmitenteParaNfe(nfe, emitente) {
    nfe['emitente']['municipio'] = emitente['municipio'];
    nfe['emitente']['bairro'] = emitente['bairro'];
    nfe['emitente']['endereco'] = emitente['endereco'];
    nfe['emitente']['cep'] = emitente['cep'];
    nfe['emitente']['nome_fantasia'] = emitente['nome_fantasia'];

    return new Promise((resolve, reject) => {
        resolve(nfe);
    });
}

async function getProdutos(page) {
    const eansComerciaisPromise = getXpathContent(page, `//div[@id='Prod']/div/table/tbody/tr/td[@class='table_produtos']/table/tbody/tr/td/table/tbody/tr[@class='col-3']/td[1]/span[@class='linha']`);
    const eansTributaveisPromise = getXpathContent(page, `//div[@id='Prod']/div/table/tbody/tr/td[@class='table_produtos']/table/tbody/tr/td/table[2]/tbody/tr[3]/td[1]/span[@class='linha']`);
    const cfopsPromise = getXpathContent(page, `//div[@id='Prod']/div/table/tbody/tr/td[@class='table_produtos']/table/tbody/tr/td/table[1]/tbody/tr[2]/td[2]/span[@class='linha']`);
    const qtdsPromise = getSelectorContent(page, "table.textoArial8:nth-child(1) div.Formulario:nth-child(2) td.table_produtos table.toggle tbody:nth-child(1) tr.highlighted td.fixo-prod-serv-qtd > span.linha:nth-child(2)");
    const unidadesComerciaisPromise = getSelectorContent(page, "table.textoArial8:nth-child(1) div.Formulario:nth-child(2) td.table_produtos table.toggle tbody:nth-child(1) tr.highlighted td.fixo-prod-serv-uc > span.linha:nth-child(2)");
    const valoresPromise = getSelectorContent(page, "table.textoArial8:nth-child(1) div.Formulario:nth-child(2) td.table_produtos table.toggle tbody:nth-child(1) tr.highlighted td.fixo-prod-serv-vb > span.linha:nth-child(2)");
    const valoresUnitariosPromises = getSelectorContent(page, "table.textoArial8:nth-child(1) div.Formulario:nth-child(2) td.table_produtos table.toggable table:nth-child(2) tbody:nth-child(1) tr:nth-child(4) td:nth-child(1) > span.linha:nth-child(2)");
    const codigosPromise = getSelectorContent(page, "table.textoArial8:nth-child(1) div.Formulario:nth-child(2) td.table_produtos table.toggable table:nth-child(1) tbody:nth-child(1) tr.col-4:nth-child(1) td:nth-child(1) > span.linha:nth-child(2)");
    const ncmsPromise = getSelectorContent(page, "table.textoArial8:nth-child(1) div.Formulario:nth-child(2) td.table_produtos table.toggable table:nth-child(1) tbody:nth-child(1) tr.col-4:nth-child(1) td:nth-child(2) > span.linha:nth-child(2)");
    const descontosPromises = getSelectorContent(page, "table.textoArial8:nth-child(1) div.Formulario:nth-child(2) td.table_produtos table.toggable table:nth-child(1) tbody:nth-child(1) tr:nth-child(3) td:nth-child(1) > span.linha:nth-child(2)");
    const descricoesPromise = getSelectorContent(page, "table.textoArial8:nth-child(1) div.Formulario:nth-child(2) td.table_produtos table.toggle tbody:nth-child(1) tr.highlighted td.fixo-prod-serv-descricao > span.multiline:nth-child(2)");

    const descricoes = await descricoesPromise;
    const descontos = await descontosPromises;
    const ncms = await ncmsPromise;
    const codigos = await codigosPromise;
    const valoresUnitarios = await valoresUnitariosPromises;
    const valores = await valoresPromise;
    const unidadesComerciais = await unidadesComerciaisPromise;
    const qtds = await qtdsPromise;
    const cfops = await cfopsPromise;
    const eansTributaveis = await eansTributaveisPromise;
    const eansComerciais = await eansComerciaisPromise;

    // console.log('10b');
    const produtos = [];
    // console.log('descricoes.length: ', descricoes.length);
    for (let index = 0; index < descricoes.length; index++) {
        produtos.push({
            'descricao': descricoes[index],
            'qtd': qtds[index],
            'unidade_comercial': unidadesComerciais[index],
            'valor': valores[index],
            'valor_unitario': valoresUnitarios[index],
            'codigo': codigos[index],
            'ncm': ncms[index],
            'cfop': cfops[index],
            'ean_comercial': eansComerciais[index],
            'ean_tributavel': eansTributaveis[index],
            'desconto': descontos[index]
        })
    }
    // console.log('10c');
    return new Promise((resolve, reject) => {
        resolve(produtos);
    });
}

module.exports.getNFEData = async (url) => {
    const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
    const page = await browser.newPage();
    await page.setViewport({
        width: 1024,
        height: 860
    });
    await page.goto(url);
    try {
        let [response] = await Promise.all([
            page.waitForNavigation(),
            page.click('#btn_visualizar_abas')
        ]);
    } catch (error) {
        throw {message: NOT_FOUND_INVOICE_ERROR_MSG, code: 'NOT_FOUND_BUTTON'};
    }

    let nfe = await getNfe(page);
    // console.log('4');

    await page.click('#btn_aba_emitente')
    // console.log('5');
    const razaoSocialSelector = "#Emitente > table:nth-child(2) > tbody > tr.col-2 > td:nth-child(1) > span";
    await page.waitForSelector(razaoSocialSelector);
    // console.log('6');
    const emitente = await getEmitenteData(page);
    // console.log('7');
    nfe = await mergeEmitenteParaNfe(nfe, emitente);
    // console.log('8');
    [response] = await Promise.all([
        page.click('#btn_aba_produtos'),
        page.waitForNavigation({ waitUntil: 'networkidle2' }),
    ]);
    // console.log('9');
    // const firstProductNumberSelector = "#Prod > div > table:nth-child(227)";
    // await page.waitForSelector(firstProductNumberSelector);
    // console.log('10');

    const produtos = await getProdutos(page);
    // console.log('11');
    nfe['produtos'] = produtos;
    nfe['qtdProdutos'] = produtos.length;
    nfe['url'] = url;
    
    // console.log(nfe);
    
    await browser.close();
    // return produtos;
    return nfe;
}